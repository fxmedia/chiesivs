// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PollObject.h"
#include "VRBANISM_0043/Websocket/ReplyLiveStructs.h"
#include "PollManager.generated.h"

UCLASS()
class VRBANISM_0043_API APollManager : public AActor
{
	GENERATED_BODY()

public:  // Public variables
	// Poll object
	UPROPERTY(BlueprintReadWrite, EditInstanceOnly, Category = Poll)
	APollObject* PollObject;

	// The interval between API queries to check for new answers
	UPROPERTY(BlueprintReadWrite, EditInstanceOnly, Category = Poll)
	float PollRate = 5.0f;

public:  // Public functions
	// Sets default values for this actor's properties
	APollManager();

	UFUNCTION(BlueprintCallable)
	void StartPoll(FReplyLivePollData PollData);

	UFUNCTION(BlueprintCallable)
	void PauzePoll(FReplyLivePollData PollData);
	
	UFUNCTION(BlueprintCallable)
	void StopPoll(FReplyLivePollData PollData);

	UFUNCTION(BlueprintCallable)
	void ClearPoll(FReplyLivePollData PollData);

	UFUNCTION(BlueprintCallable)
	void GetPollAnswers(FReplyLiveAPIPollAnswers PollAnswers);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
