// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Actors/NDIReceiveActor.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "VRBANISM_0043/Websocket/ReplyLiveStructs.h"
#include "SceneManager.generated.h"

UCLASS()
class VRBANISM_0043_API ASceneManager : public AActor
{
	GENERATED_BODY()

public:  // Public variables
	// Array of all hosts and speakers in the level
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = SceneElements)
	TArray<ANDIReceiveActor*> HostsAndSpeakers;

public:  // Public functions
	// Update the visibility of hosts and speakers
	UFUNCTION(BlueprintCallable)
	void UpdateScene(FReplyLiveLocationData data);

public:	
	// Sets default values for this actor's properties
	ASceneManager();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
